package com.github.codeinnovator.qgenie.controller;

import java.util.HashMap;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @GetMapping(value = "/api/greet", produces = MediaType.APPLICATION_JSON_VALUE)
    public HashMap<String, Object> greet() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("status", "success");
        return result;
    }
}