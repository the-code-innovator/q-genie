package com.github.codeinnovator.qgenie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QGenieApplication {

	public static void main(String[] args) {
		SpringApplication.run(QGenieApplication.class, args);
	}

}
